// @flow

import React from 'react';
import { render } from 'react-dom';
import App from 'components/organisms/App';
// import registerServiceWorker from 'utils/sw/registerServiceWorker';
import './index.scss';


const woffAndSocialDescription =
  "Technical Assessment";

const config: ArticleConfig = {
  masthead: 'smh',
  title: "Technical Assessment",
  woff: woffAndSocialDescription,
  topic: 'Iditorial',
  topicName: 'Iditorial'
};

render(<React.Fragment>
  <App {...config} />
</React.Fragment>, document.getElementById('app'));


//registerServiceWorker();
