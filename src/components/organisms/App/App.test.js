import React from 'react';
import App from './index';

it('App renders without crashing', () => {
  const component = shallow(<App />);
  expect(component).toMatchSnapshot();
});
