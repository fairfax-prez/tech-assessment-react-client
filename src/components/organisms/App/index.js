// @flow

import React, { Component } from 'react';
import HeaderNav from 'components/molecules/HeaderNav';
import debounce from 'just-debounce-it';
import { select } from 'd3-selection';
import SvgIcons from 'components/molecules/SvgIcons';
import './App.scss';


class App extends Component<ArticleConfig, State> {

  constructor(props: ArticleConfig) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    select(window).on(
      'resize.App',
      debounce(this.checkForChangeInScreenSize, 300)
    );
  }

  checkForChangeInScreenSize = () => {
    this.setState({ screenWidth: window.innerWidth, screenHeight: window.innerHeight });
  };


  render() {
    const {
    } = this.props;
    return (
      <div className="app">
        <SvgIcons loaders social  />
        <HeaderNav masthead="smh" />

        <div className="article-section"  >
          <div className="article-section__subsection">
            <p>Your Iris component goes here</p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
