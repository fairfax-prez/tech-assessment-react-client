// @flow

import React from 'react';
import './Loader.scss';

type Props = {
  mastheadAbbrev: string,
  failed?: boolean,
  loaderStyle?: string,
  onRetry?: () => void
};

type RetryButtonProps = {
  onRetry?: () => void
};

const styleToClass = style => {
  switch (style) {
    case 'standard':
      return 'standardLoader';
    case 'dark':
      return 'standardDarkLoader';
    default:
      return 'standardLoader';
  }
};

const Loader = ({ mastheadAbbrev, loaderStyle, onRetry, failed }: Props) => (
  <div className="loaderContainer">
    <div className={styleToClass(loaderStyle)}>
      <div className="loader-icon">
        <div className="loader-icon__borders-container">
          <div className="loader-icon__borders-container__borders" />
        </div>
        <svg
          viewBox="0 0 60 60"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          className="loader-icon__svg"
        >
          <use
            xmlnsXlink="http://www.w3.org/1999/xlink"
            xlinkHref={`#icon-loading-${mastheadAbbrev}`}
            className="loader-icon__svg__image"
          />
        </svg>
      </div>
      <p className="loader-message">Loading...</p>
      {failed ? <RetryButton onRetry={onRetry} /> : ''}
    </div>
  </div>
);

const RetryButton = ({ onRetry }: RetryButtonProps) => (
  <div className="loader-retry">
    <button className="loader-retry__button" type="button" onClick={onRetry}>
      Try again
    </button>
  </div>
);

Loader.defaultProps = {
  loaderStyle: 'standard',
  failed: false,
  onRetry: () => {}
};

RetryButton.defaultProps = {
  onRetry: () => {}
};

export default Loader;
