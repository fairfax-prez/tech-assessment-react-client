// @flow

import React from 'react';
import './Credits.scss';

type Contributor = {
  name: string,
  role: string
}

type Props = {
  contributors: Array<Contributor>
}

const Credits = ({ contributors }: Props) => (
  <article className="article-credits article-section__subsection">
    <h2 className="article-credits__header">Credits</h2>
    <ul className="article-credits__list">
      {contributors.map(({ role, name, link }) => (
        <li className="article-credits__list__item" key={name}>
          <span className="article-credits__list__item__role">{role}</span>{' '}
          {link &&
            <span className="article-credits__list__item__name" ><a href={link} >{name}</a></span>
          }
          {!link && 
            <span className="article-credits__list__item__name"  >{name}</span>
          }
        </li>
      ))}
    </ul>
  </article>
);

export default Credits;
