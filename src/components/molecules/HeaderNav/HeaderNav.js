// @flow

import React, { Component } from 'react';
import './HeaderNav.scss';

type Props = {
  backLink?: string,
  customStyles: any,
  masthead: string
};

const OFFSET_BOTTOM = 30;
const OFFSET = 300;

class HeaderNav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      headerOpacity: 1
    }
  }


  componentDidMount() {
    this.handleOnWindowScroll()
    window.addEventListener('scroll', this.handleOnWindowScroll, false);
  }

  /* Show Hide Nav bar on Scroll */
  lastTop; currentScrollTop = 0;
  handleOnWindowScroll = (event) => {
    const navbar = document.getElementById("navheader");

    var winY = window.scrollY;
    var navTop = 56;// navbar.getBoundingClientRect().height;

    this.currentScrollTop = winY;

    if (this.lastTop < this.currentScrollTop && winY > navTop + navTop) {
      navbar.classList.add("scrollUp");
    } else if (this.lastTop > this.currentScrollTop && !(winY <= navTop)) {
      navbar.classList.remove("scrollUp");
    }
    this.lastTop = this.currentScrollTop;
  }



  render() {
    const { backLink, customStyles, masthead } = this.props;
    const headerStyle = { ...customStyles, opacity: this.state.headerOpacity }
    let navLink = backLink;

    let onClickHeaderNavFunction;

    if (navLink === undefined) {
      navLink = `https://www.${masthead}.com.au/`;
    }

    if (onClickHeaderNavFunction === undefined) {
      onClickHeaderNavFunction = () => {
        window.location.href = navLink;
      };
    }

    return (
      <header className="header-nav" style={headerStyle} id="navheader">
        <div className={`header-nav__logo is--${masthead}`}>
          <div
            className="header-nav__logo__image"
            style={{ backgroundImage: `url(images/logos/${masthead}.svg)` }}
            onClick={onClickHeaderNavFunction}
          />
          <a className="header-nav__logo__link" href={navLink} />
        </div>
  
      </header>
    )
  }
}


export default HeaderNav;
