// @flow

import React from 'react';
import SocialMediaBar from 'components/molecules/SocialMediaBar';
import './Footer.scss';

type Props = {
  masthead: string,
  screenSize: string,
  socialShareUrl: string,
  description: string,
  initFacebookLibrary: boolean
};

const Footer = ({
  description,
  initFacebookLibrary,
  shareUrl,
  masthead,
  screenSize,
  title
}: Props) => (
  <footer className="footer">
    <header className="footer__header article-section__super-subsection">
      <div className="footer__header__logo-container">
        <div className={`footer__header__logo-container__logo is--${masthead}`}>
          <div
            className="footer__header__logo-container__logo__image"
            style={{
              backgroundImage: `url(images/logos/${masthead}.svg)`
            }}
          />
          {/* eslint-disable-next-line */}
          <a
            className="footer__header__logo-container__logo__link"
            href={`https://www.${masthead}.com.au`}
          />
        </div>
      </div>

      <SocialMediaBar
        screenSize={screenSize}
        shareUrl={shareUrl}
        link={shareUrl}
        title={title}
        description={description}
        initFacebookLibrary={initFacebookLibrary}
        customContainerClassName="socialButtonContainer footer__header__social"
        customButtonClassName="socialButton footer__header__social__button"
      />
    </header>

    <div className="footer__copyright article-section__super-subsection">
      <div className="footer__copyright__text">Copyright © {new Date().getFullYear()}</div>
    </div>
  </footer>
);

export default Footer;
